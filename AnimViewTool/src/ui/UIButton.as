package ui
{
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	
	import org.osflash.signals.Signal;
	
	public class UIButton extends Sprite
	{
		private var _bg:DisplayObject;
		private var _mouseArea:Sprite;
		private var _enabled:Boolean= true;
		
		public var clicked:Signal;
		
			
		public function UIButton(bmp:BitmapData)
		{
			super();
			
			clicked = new Signal(String);
			drawFace(bmp);
			
			buttonMode= true;
			addEventListener(MouseEvent.CLICK, onClick);
		}
		
		private function drawFace(faceBmp:BitmapData):void
		{
			
			graphics.clear();
			
			graphics.beginBitmapFill(faceBmp);
			graphics.drawRect(0, 0, faceBmp.width, faceBmp.height);
			graphics.endFill();
		}
		
		protected function onClick(event:MouseEvent):void
		{
			clicked.dispatch(this.name);
			event.stopImmediatePropagation();
		}
		
		public function get enabled():Boolean  { return _enabled; }
		
		public function set enabled(val:Boolean):void
		{
			_enabled = val;
			if (_enabled) {
				alpha = 1;
				blendMode= BlendMode.NORMAL;
				mouseEnabled = true;
				mouseChildren = true;
			}
			else {
				alpha = 0.55;
				blendMode= BlendMode.SUBTRACT;
				mouseEnabled = false;
				mouseChildren = false;
			}
		}
	}
}